"""This is Infin's initialization script.
It handles connecting to Discord and loading commands.
"""
# Copyright 2018 Sahibdeep Nann
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#     http://www.apache.org/licenses/LICENSE-2.0
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import argparse
import asyncio
import datetime
import logging
import os
import sys
from colorlog import ColoredFormatter
from discord import Game
from discord.ext import commands
from dotenv import load_dotenv

ARGUMENT_PARSER = argparse.ArgumentParser(description='A fast, open Discord bot written in Python.')
ARGUMENT_PARSER.add_argument('-v', '--verbose', help='set logging level to DEBUG',
                             action='store_const',
                             const=logging.DEBUG,
                             default=logging.INFO, dest='logging')

load_dotenv()

if __name__ == '__main__':
    LOOP = asyncio.get_event_loop()

    ARGS = ARGUMENT_PARSER.parse_args(sys.argv[1:])

    LOGGING_LEVEL = ARGS.logging

    logging.root.setLevel(LOGGING_LEVEL)
    LOG_FORMATTER = ColoredFormatter(
        '%(name)s %(log_color)s%(levelname)s%(reset)s: %(log_color)s%(message)s%(reset)s')
    LOG_HANDLER = logging.StreamHandler()
    LOG_HANDLER.setLevel(LOGGING_LEVEL)
    LOG_HANDLER.setFormatter(LOG_FORMATTER)
    LOG = logging.getLogger('core.init')
    LOG.setLevel(LOGGING_LEVEL)
    LOG.addHandler(LOG_HANDLER)
    LOG.info('Starting Infin 1.0.0')
    LOG.debug('Running in verbose mode')

    async def on_command_error(ctx, error):
        """Properly logs command errors."""
        if ctx.command is None:
            logger = logging.getLogger('cmds.cnf')
        else:
            logger = logging.getLogger('cmds.' + ctx.command.name)
        logger.setLevel(LOGGING_LEVEL)
        logger.addHandler(LOG_HANDLER)
        logger.error('%s: %s', error.__class__.__name__, error)
    BOT = commands.Bot(command_prefix='infin ',
                       description='A fast, open Discord bot written in Python.',
                       activity=Game('gitlab.com/initializesahib/infin'),
                       pm_help=True)
    BOT.uptime = datetime.datetime.now() # This will be used in the info command later.
    BOT.on_command_error = on_command_error
    BOT.log_handler = LOG_HANDLER
    BOT.logging_level = LOGGING_LEVEL
    LOG.debug('Created bot object')

    @BOT.event
    async def on_message(msg):
        """Required for help command reaction."""
        if msg.content.startswith('infin help'):
            await msg.add_reaction('📫')
        await BOT.process_commands(msg)

    @BOT.event
    async def on_ready():
        """Notifies user that bot is connected."""
        LOG.info('Connected to Discord')
    LOG.debug('Attached on_ready event')

    COMMAND_FILES = os.listdir('cmds')
    COMMAND_NAMES = [filename.split('.')[0] for filename in COMMAND_FILES]
    COMMANDS = ['cmds.' + command_name for command_name in COMMAND_NAMES]
    LOG.debug('Loaded commands list')

    for command in COMMANDS:
        if command == 'cmds.__pycache__':
            break
        command_logger = logging.getLogger(command)
        command_logger.setLevel(LOGGING_LEVEL)
        command_logger.addHandler(LOG_HANDLER)
        BOT.load_extension(command)
        command_logger.info('Command loaded')

    try:
        LOOP.run_until_complete(BOT.start(os.getenv('TOKEN')))
    except KeyboardInterrupt:
        LOG.warning('Logging out of Discord')
        LOOP.run_until_complete(BOT.logout())
    finally:
        LOOP.close()
        LOG.critical('Thank you and goodbye')
        sys.exit(0)
