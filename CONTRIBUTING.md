# Contributing

When contributing to this repository, please first discuss the change you wish to make via issue 
or any other method with the owner of this repository before making a change. 

Make sure that your code integrates well with the other code in the repository.  

Don't break anything.  

You don't need to contribute but if you do it'll be cool I guess.  

You can add your copyright notice to the code like so:

```
# Copyright 2018 Sahibdeep Nann
# Modified work copyright [year] [your name]
```

You **must** retain my copyright notice on all files.  

