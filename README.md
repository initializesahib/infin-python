# Infin
## v1.0.0
**Requirements**:
- `libffi-dev` and `libnacl-dev` (package names may differ on other distros)  
- `ffmpeg`  
- Either [Anaconda](https://www.anaconda.com) or [Miniconda](https://conda.io/miniconda.html)  
  
**Setup**:
1. Clone the repository: `git clone https://gitlab.com/initializesahib/infin.git`  
2. Create the environment: `cd infin && conda env create -f env.yaml`  
3. Create a file named **.env** that contains `TOKEN=xxx`, where `xxx` is the token for the Discord bot account  
4. Check the code: `pylint init.py cmds/*py`  
5. Run either `./infin` or `./infin -v` to start the bot in normal or verbose mode respectively  
