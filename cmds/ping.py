# Copyright 2018 Sahibdeep Nann
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#     http://www.apache.org/licenses/LICENSE-2.0
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from discord.ext import commands

class PingCommand:
    """
    Contains the ping command.
    """
    def __init__(self, bot):
        self.bot = bot

    @commands.command(name='ping', description='Checks latency between your message and the bot.')
    @commands.guild_only()
    async def ping(self, ctx):
        """
        Pong!
        """
        sent_message = await ctx.send(':ping_pong: in **...** ms')
        message_latency = (sent_message.created_at - ctx.message.created_at).total_seconds() * 1000
        await sent_message.edit(content=f':ping_pong: in **{message_latency}** ms')

def setup(bot):
    bot.add_cog(PingCommand(bot))
