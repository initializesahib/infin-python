# Copyright 2018 Sahibdeep Nann
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#     http://www.apache.org/licenses/LICENSE-2.0
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import inspect
import os
from discord.ext import commands

class SourceCommand:
    """
    Contains the source command.
    """
    def __init__(self, bot):
        self.bot = bot

    @commands.command(name='source', description='Shows source code.')
    @commands.guild_only()
    async def source(self, ctx, cmd = None):
        """
        Shows the source code of the bot.
        """
        repo_url = 'https://gitlab.com/initializesahib/infin'
        if cmd is None:
            return await ctx.send(repo_url)
        command = self.bot.get_command(cmd)
        if command is None:
            await ctx.send(f'🚫 **could not find** {cmd}')
            raise commands.errors.CommandNotFound(f'The command {cmd} does not exist.')
            return
        source_code = command.callback.__code__
        line_num, first_line_num = inspect.getsourcelines(source_code)
        if not command.callback.__module__.startswith('discord'):
            code_file = os.path.relpath(source_code.co_filename).replace('\\', '/')
        else:
            code_file = command.callback.__module__.replace('.', '/') + '.py'
            repo_url = 'https://github.com/Rapptz/discord.py'
        if repo_url == 'https://github.com/Rapptz/discord.py':
            code_url = f'{repo_url}/blob/rewrite/{code_file}#L{first_line_num}-L{first_line_num + len(line_num) - 1}'
        else:
            code_url = f'{repo_url}/blob/master/{code_file}#L{first_line_num}'
        await ctx.send(f'✅ **code**: <{code_url}>')

def setup(bot):
    bot.add_cog(SourceCommand(bot))

