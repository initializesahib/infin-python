# Copyright 2018 Sahibdeep Nann
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#     http://www.apache.org/licenses/LICENSE-2.0
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import logging
from discord.ext import commands

class PurgeCommand:
    """
    Contains the purge command.
    """
    def __init__(self, bot):
        self.bot = bot

    @commands.command(name='purge', description='Purges a set amount of messages from the channel. Requires the Message Manages permission.')
    @commands.guild_only()
    @commands.has_permissions(manage_messages=True)
    async def purge(self, ctx, num: int):
        """
        Purges messages.
        """
        await ctx.message.channel.purge(limit=num)
        log = logging.getLogger('cmds.purge')
        log.setLevel(self.bot.logging_level)
        log.addHandler(self.bot.log_handler)
        log.debug(f'Deleted {num} messages from channel {ctx.message.channel.name} in guild {ctx.message.guild.name}')

def setup(bot):
    bot.add_cog(PurgeCommand(bot))
