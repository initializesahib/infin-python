# Copyright 2018 Sahibdeep Nann
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#     http://www.apache.org/licenses/LICENSE-2.0
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import random
from discord.ext import commands

class RollCommand:
    """
    Contains the roll command.
    """
    def __init__(self, bot):
        self.bot = bot

    @commands.command(name='roll', description='Roll a die and test your luck.')
    @commands.guild_only()
    async def roll(self, ctx):
        """
        Roll a die.
        """
        rolled_num = random.randrange(1, 7)
        await ctx.send(f':game_die: rolled a **{rolled_num}**')

def setup(bot):
    bot.add_cog(RollCommand(bot))

