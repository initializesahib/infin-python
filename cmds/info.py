# Copyright 2018 Sahibdeep Nann
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#     http://www.apache.org/licenses/LICENSE-2.0
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import datetime
import discord
from discord.ext import commands

class InfoCommand:
    """
    Contains the info command.
    """
    def __init__(self, bot):
        self.bot = bot

    @commands.command(name='info', description='Displays information about the bot in an embed.')
    @commands.guild_only()
    async def info(self, ctx):
        """
        Shows bot info in a neat embed.
        """
        info_embed = discord.Embed(title='Bot Information',
                                   type='rich',
                                   description='Here is some info about Infin.',
                                   url='https://www.gitlab.com/initializesahib/infin',
                                   timestamp=datetime.datetime.now(),
                                   colour=discord.Colour.from_rgb(28, 138, 219))
        info_embed.set_author(name='Infin', icon_url=self.bot.user.avatar_url)
        info_embed.add_field(name='Version', value='v1.0.0')
        info_embed.add_field(name='Library', value='[discord.py 1.0.0a](https://www.github.com/Rapptz/discord.py/blob/rewrite)')
        info_embed.add_field(name='Help Command', value='infin help')
        info_embed.add_field(name='Created By', value='InitializeSahib: [GitLab](https://www.gitlab.com/initializesahib) [Twitter](https://www.twitter.com/SahibPrime)')
        uptime_minutes, uptime_seconds = divmod((datetime.datetime.now() - self.bot.uptime).total_seconds(), 60)
        info_embed.add_field(name='Uptime', value=f'{round(uptime_minutes)}m{round(uptime_seconds)}s')
        await ctx.send(embed=info_embed)        

def setup(bot):
    bot.add_cog(InfoCommand(bot))
