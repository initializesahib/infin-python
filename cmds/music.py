# Copyright 2018 Sahibdeep Nann
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#     http://www.apache.org/licenses/LICENSE-2.0
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import asyncio
import discord
from discord.ext import commands
import youtube_dl

youtube_dl.utils.bug_reports_message = lambda: ''
ytdl_format_options = {
    'format': 'm4a/bestaudio/best',
    'outtmpl': '%(title)s.%(ext)s',
    'restrictfilenames': True,
    'noplaylist': True,
    'nocheckcertificate': True,
    'ignoreerrors': False,
    'logtostderr': False,
    'quiet': True,
    'no_warnings': True,
    'default_search': 'auto',
    'source_address': '0.0.0.0'
}
ffmpeg_options = {
    'before_options': '-nostdin',
    'options': '-vn'
}
youtube = youtube_dl.YoutubeDL(ytdl_format_options)

class YouTubeSource(discord.PCMVolumeTransformer):
    def __init__(self, src, *, data, volume=1):
        super().__init__(src, volume)
        self.data = data
        self.title = data.get('title')
        self.url = data.get('url')
    @classmethod
    async def from_url(cls, url, *, loop=None):
        loop = loop or asyncio.get_event_loop()
        data = await loop.run_in_executor(None, lambda: 
youtube.extract_info(url, download=False))
        if 'entries' in data:
            data = data['entries'][0]
        filename = data['url']
        return cls(discord.FFmpegPCMAudio(filename, **ffmpeg_options), data=data)

class MusicCommands:
    """
    Contains music commands.
    """
    def __init__(self, bot):
        self.bot = bot

    @commands.command(name='play', description='Plays the given URL or search term from YouTube.')
    async def play(self, ctx, *, url):
        """
        Plays something from YouTube.
        """
        async with ctx.typing():
            player = await YouTubeSource.from_url(url, loop=self.bot.loop)
            ctx.voice_client.play(player)
        await ctx.send(f':radio: **playing** {player.title}')

    @commands.command(name='volume', description='Changes the volume of the currently playing song.')
    async def volume(self, ctx, volume: int):
        """
        Changes the volume.
        """
        if ctx.voice_client is None:
            await ctx.send('🚫 not connected **to a voice channel**')
            raise commands.CommandError('Not connected to a voice channel.')
            return
        ctx.voice_client.source.volume = volume / 100
        await ctx.send(f'✅ volume at **{volume}%**')

    @commands.command(name='stop', description='Stops playback and disconnects from the voice channel.')
    async def stop(self, ctx):
        """
        Stops playing.
        """
        await ctx.voice_client.disconnect()
        await ctx.send('✅ **stopped** playback')

    @commands.command(name='summon', description='Joins the voice channel of whoever runs it.')
    @commands.guild_only()
    @play.before_invoke
    async def summon(self, ctx):
        """
        Joins your voice channel.
        """
        if ctx.voice_client is None:
            if ctx.author.voice:
                await ctx.author.voice.channel.connect()
                await ctx.send('✅ **joined** voice channel')
            else:
                await ctx.send('🚫 you are not **in a voice channel**')
                raise commands.CommandError('User is not in a voice channel.')

def setup(bot):
    bot.add_cog(MusicCommands(bot))
