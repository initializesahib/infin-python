# Copyright 2018 Sahibdeep Nann
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#     http://www.apache.org/licenses/LICENSE-2.0
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import logging
import discord
from discord.ext import commands

class ControlCommand:
    """
    Contains the ctl command.
    """
    def __init__(self, bot):
        self.bot = bot

    @commands.command(name='ctl',
                      description="""Modifies and sets different configuration values in Infin. 
                                     Restricted to the owner.""")
    @commands.is_owner()
    @commands.guild_only()
    async def ctl(self, ctx, *args):
        """
        Changes values of the bot internals.
        """
        log = logging.getLogger('cmds.ctl')
        log.setLevel(self.bot.logging_level)
        log.addHandler(self.bot.log_handler)
        if args[0] == 'set':
            if args[1] == 'presence.status':
                if args[2] == 'online':
                    await self.bot.change_presence(status=discord.Status.online,
                                                   activity=discord.Game(('gitlab.com/'
                                                                          'initializesahib/infin'
                                                                         )))
                    await ctx.send(f'✅ **presence.status** == online')
                    log.debug('presence.status changed to online')
                elif args[2] == 'dnd':
                    await self.bot.change_presence(status=discord.Status.dnd, activity=discord.Game('gitlab.com/initializesahib/infin'))
                    await ctx.send(f'✅ **presence.status** == dnd')
                    log.debug('presence.status changed to dnd')
                elif args[2] == 'idle':
                    await self.bot.change_presence(status=discord.Status.idle, activity=discord.Game('gitlab.com/initializesahib/infin'))
                    await ctx.send(f'✅ **presence.status** == idle')
                    log.debug('presence.status changed to idle')
                elif args[2] == 'invisible':
                    await self.bot.change_presence(status=discord.Status.invisible)
                    await ctx.send(f'✅ **presence.status** == invisible')
                    log.debug('presence.status changed to invisible')
                else:
                    await ctx.send(f'🚫 **discord.Status.{args[2]}** does not exist')
                    raise commands.CommandError('Invalid Discord status')
            elif args[1] == 'presence.game':
                new_game = ' '.join(args[2:])
                if new_game == 'none':
                    await self.bot.change_presence(status=ctx.me.status)
                    return await ctx.send(f'✅ **presence.game** == None')
                await self.bot.change_presence(status=ctx.me.status, activity=discord.Game(new_game))
                await ctx.send(f'✅ **presence.game** == {new_game}')
            else:
                await ctx.send(f'🚫 **ControlCommand.ctl.set** does not contain functionality for **{args[1]}**')
                raise commands.CommandError('Key does not exist')
        elif args[0] == 'load':
            try:
                self.bot.load_extension('cmds.' + args[1])
                await ctx.send(f'✅ **loaded** {args[1]}')
                log = logging.getLogger('cmds.' + args[1])
                log.setLevel(self.bot.logging_level)
                log.addHandler(self.bot.log_handler)
                log.info('Command loaded')
            except (ImportError, discord.ClientException) as e:
                await ctx.send(f'🚫 **failed to load** {args[1]}')
                raise e
                return
        elif args[0] == 'unload':
            if args[1] == 'ctl':
                await ctx.send('🚫 **cannot unload** ctl')
                log.warning("You really shouldn't be trying to unload ctl")
                return
            self.bot.unload_extension('cmds.' + args[1])
            await ctx.send(f'✅ **unloaded** {args[1]}')
            log = logging.getLogger('cmds.' + args[1])
            log.setLevel(self.bot.logging_level)
            log.addHandler(self.bot.log_handler)
            log.info('Command unloaded')
        elif args[0] == 'reload':
            try:
                self.bot.unload_extension('cmds.' + args[1])
                self.bot.load_extension('cmds.' + args[1])
                await ctx.send(f'✅ **reloaded** {args[1]}')
                log = logging.getLogger('cmds.' + args[1])
                log.setLevel(self.bot.logging_level)
                log.addHandler(self.bot.log_handler)
                log.info('Command reloaded')
            except (ImportError, discord.ClientException) as e:
                await ctx.send(f'🚫 **failed to reload** {args[1]}')
                raise e
                return
        else:
            await ctx.send(f'🚫 **ControlCommand.ctl** does not contain functionality for **{args[0]}**')
            raise commands.CommandError('Subcommand does not exist')
            return

def setup(bot):
    bot.add_cog(ControlCommand(bot))
